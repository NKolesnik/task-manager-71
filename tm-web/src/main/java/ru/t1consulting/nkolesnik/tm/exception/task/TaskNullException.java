package ru.t1consulting.nkolesnik.tm.exception.task;

import ru.t1consulting.nkolesnik.tm.exception.AbstractException;

public final class TaskNullException extends AbstractException {

    public TaskNullException() {
        super("Error! Task is null...");
    }

}
