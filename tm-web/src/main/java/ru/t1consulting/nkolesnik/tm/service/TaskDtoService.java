package ru.t1consulting.nkolesnik.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1consulting.nkolesnik.tm.api.repository.dto.ITaskDtoRepository;
import ru.t1consulting.nkolesnik.tm.api.service.ITaskDtoService;
import ru.t1consulting.nkolesnik.tm.exception.task.TaskEmptyIdException;
import ru.t1consulting.nkolesnik.tm.exception.task.TaskNullException;
import ru.t1consulting.nkolesnik.tm.exception.user.UserEmptyIdException;
import ru.t1consulting.nkolesnik.tm.model.dto.TaskDto;

import java.util.List;
import java.util.Optional;

@Service
public class TaskDtoService implements ITaskDtoService {

    @NotNull
    @Autowired
    private ITaskDtoRepository taskRepository;

    @Modifying
    @Transactional
    public void add(@Nullable final String userId, @Nullable final TaskDto task) {
        Optional.ofNullable(userId).orElseThrow(UserEmptyIdException::new);
        Optional.ofNullable(task).orElseThrow(TaskNullException::new);
        task.setUserId(userId);
        taskRepository.saveAndFlush(task);
    }

    @Override
    @Modifying
    @Transactional
    public TaskDto create(@Nullable final String userId) {
        Optional.ofNullable(userId).orElseThrow(UserEmptyIdException::new);
        @Nullable final TaskDto task = new TaskDto();
        task.setUserId(userId);
        task.setName("New Task " + System.currentTimeMillis());
        taskRepository.saveAndFlush(task);
        return task;
    }

    @Override
    public boolean existsById(@Nullable final String userId, @Nullable final String id) {
        Optional.ofNullable(userId).orElseThrow(UserEmptyIdException::new);
        Optional.ofNullable(id).orElseThrow(TaskEmptyIdException::new);
        return taskRepository.existsByUserIdAndId(userId, id);
    }

    @Override
    @Nullable
    public TaskDto findById(@Nullable final String userId, @Nullable final String id) {
        Optional.ofNullable(userId).orElseThrow(UserEmptyIdException::new);
        Optional.ofNullable(id).orElseThrow(TaskEmptyIdException::new);
        return taskRepository.findByUserIdAndId(userId, id);
    }

    @Override
    @Nullable
    public List<TaskDto> findAll(@Nullable final String userId) {
        Optional.ofNullable(userId).orElseThrow(UserEmptyIdException::new);
        return taskRepository.findAllByUserId(userId);
    }

    @Override
    public long count(@Nullable final String userId) {
        Optional.ofNullable(userId).orElseThrow(UserEmptyIdException::new);
        return taskRepository.countByUserId(userId);
    }

    @Override
    @Modifying
    @Transactional
    public void save(@Nullable final String userId, @Nullable final TaskDto task) {
        Optional.ofNullable(userId).orElseThrow(UserEmptyIdException::new);
        Optional.ofNullable(task).orElseThrow(TaskNullException::new);
        task.setUserId(userId);
        taskRepository.saveAndFlush(task);
    }

    @Override
    @Modifying
    @Transactional
    public void deleteById(@Nullable final String userId, @Nullable final String id) {
        Optional.ofNullable(userId).orElseThrow(UserEmptyIdException::new);
        Optional.ofNullable(id).orElseThrow(TaskEmptyIdException::new);
        taskRepository.deleteByUserIdAndId(userId, id);
    }

    @Override
    @Modifying
    @Transactional
    public void delete(@Nullable final String userId, @Nullable final TaskDto task) {
        Optional.ofNullable(userId).orElseThrow(UserEmptyIdException::new);
        Optional.ofNullable(task).orElseThrow(TaskNullException::new);
        deleteById(userId, task.getId());
    }

    @Override
    @Modifying
    @Transactional
    public void deleteAll(@Nullable final String userId, @Nullable final List<TaskDto> taskList) {
        Optional.ofNullable(userId).orElseThrow(UserEmptyIdException::new);
        Optional.ofNullable(taskList).orElseThrow(TaskNullException::new);
        taskList.forEach(task -> deleteById(userId, task.getId()));
    }

    @Override
    @Modifying
    @Transactional
    public void clear(@Nullable final String userId) {
        Optional.ofNullable(userId).orElseThrow(UserEmptyIdException::new);
        taskRepository.deleteByUserId(userId);
    }

}
