package ru.t1consulting.nkolesnik.tm.exception.user;

public final class UserEmptyPasswordException extends AbstractUserException {

    public UserEmptyPasswordException() {
        super("Error! User password is empty...");
    }

}