package ru.t1consulting.nkolesnik.tm.dto.request.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1consulting.nkolesnik.tm.dto.request.AbstractUserRequest;

@Getter
@Setter
@NoArgsConstructor
public class TaskBindToProjectRequest extends AbstractUserRequest {

    @Nullable
    private String projectId;

    @Nullable
    private String taskId;

    public TaskBindToProjectRequest(@Nullable String token) {
        super(token);
    }

    public TaskBindToProjectRequest(@Nullable String token, @Nullable String projectId, @Nullable String taskId) {
        super(token);
        this.projectId = projectId;
        this.taskId = taskId;
    }

}
